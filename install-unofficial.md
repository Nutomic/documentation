# Unofficial installations

## Git 

!> **Warning** This guide assumes that you have read the instructions in
<a href="https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md">production.md</a>
and thus omits some configurations steps (reverse proxy, database configuration, etc.). At
the end you should have a PeerTube instance running the latest development version, which can
be considered unstable.

First, go to the Peertube folder and switch to the Peertube user.

```bash
cd /var/www/peertube/versions/
sudo -u peertube -H bash
```

Then clone the git repository.

```bash
git clone https://github.com/Chocobozzz/PeerTube.git peertube-develop
cd peertube-develop/
```

It should automatically be on the `develop` branch, which you can verify with `git branch`. You can
also switch to another branch or a specific commit with `git checkout [branch or commit]`. Once you
have the correct version, run the build:

```bash
yarn install --pure-lockfile
npm run build
```

The compilation will take a long time. You can also run it on your local computer, and transfer the
entire folder to your server.

Now you should make sure to add any new config fields to your `production.yaml`. And you should make
a backup of the database:

```bash
SQL_BACKUP_PATH="backup/sql-peertube_prod-$(date -Im).bak" && \
     cd /var/www/peertube && sudo -u peertube mkdir -p backup && \
     sudo -u postgres pg_dump -F c peertube_prod | sudo -u peertube tee "$SQL_BACKUP_PATH" >/dev/null
```

Finally, update the `peertube-latest` symlink to point at the new version:

```bash
cd /var/www/peertube && \
    sudo unlink ./peertube-latest && \
    sudo -u peertube ln -s versions/peertube-develop ./peertube-latest
```

Now you just need to restart Peertube. With systemd, just run `sudo systemctl restart peertube`.

Do not try to upgrade from one development version to another by running `git pull` and `npm run build`. This
will break your website. Either switch back to a release version, or make a copy of the `peertube-develop`
folder and run the compilation there.

## ArchLinux

On Arch Linux, PeerTube can be installed through the Arch User Repository thanks to a __community package__ made by [daftaupe](https://aur.archlinux.org/packages/peertube/).
 
```sh
asp checkout peertube
cd peertube
makepkg --syncdeps --rmdeps --install --clean
```

Or

```sh
yay -S peertube
```

#### Configuration

You now have to [configure the database](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md#database)
and credentials to it in the configuration file of PeerTube
in `/usr/share/webapps/peertube/config/production.yaml`.

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Arch packages available for RC or nightly builds of PeerTube. Please use the tarball:
{% include_relative installations/tarball.md %}
</div>


## CentOS

On CentOS, Fedora and RHEL, you can install PeerTube via a __community package__ made by [daftaupe](https://copr.fedorainfracloud.org/coprs/daftaupe/peertube/) on COPR.

```sh
dnf copr enable daftaupe/peertube
```

#### Prerequisites

You will need PostgreSQL, Node.JS and FFMpeg :

* Fedora you need RPM Fusion repository enabled
  ```sh
  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  ```
* CentOS you will need EPEL and the unofficial EPEL-multimedia repositories enabled
  ```sh
  cd /etc/yum.repos.d && curl -O https://negativo17.org/repos/epel-multimedia.repo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm https://negativo17.org/repos/epel-multimedia.repo
  ```

**Setup the database**

```sh
su - postgres
initdb
createuser peertube -W
createdb -O peertube peertube_prod
echo "host peertube_prod peertube 127.0.0.1/32 md5" >> data/pg_hba.conf
```

```sh
systemctl reload postgresql
```

**Start the services**

```sh
systemctl start redis
```

**Edit the configuration to fit your needs**

```sh
vim /etc/peertube/production.yaml
```

**Start PeerTube and get the initial root / password**

```sh
systemctl start peertube && journalctl -f -u peertube
```


## openSUSE 

On openSUSE, you can install PeerTube via our RPM package repository.

```bash
sudo zypper ar -f https://pkg.rigelk.eu/rpm/ peertube
sudo zypper in peertube
```

## YunoHost

On Debian running YunoHost, you can install Yarn, Node and PeerTube in one shot via a __community package__.

[![Install Peertube with YunoHost](https://install-app.yunohost.org/install-with-yunohost.png)](https://install-app.yunohost.org/?app=peertube)

See [here](https://github.com/YunoHost-Apps/peertube_ynh) for support.

## Ansible on Debian

This is an alternate way of installing Peertube on Debian, provided by a community member.

The ansible playbook triggers the full installation of Peertube **without certbot** in a **reverse proxy** scenario, meaning HTTPS is not directly handled on the Peertube server. 

This is for **Debian only**. All you need is to set the hostname, full qualified domain name, and wait about 15 minutes for the installation to go through.

 * **Repository:** [https://framagit.org/jeromeavond/ansible-role-peertube/tree/master](https://framagit.org/jeromeavond/ansible-role-peertube/tree/master)
 * **HOWTO:** [https://framagit.org/jeromeavond/ansible-role-peertube/blob/master/HOWTO.md](https://framagit.org/jeromeavond/ansible-role-peertube/blob/master/HOWTO.md)


## Ansible and Docker Compose

This is a project that lets you automatically install Peertube with Ansible, using docker-compose.

The project is maintained by [@nutomic](https://github.com/Nutomic) and used on [peertube.social](https://peertube.social).

Features:
- automatic Let's Encrypt certificate handling via Traefik
- file caching with nginx (to limit backend access and Peertube CPU usage)
- email sending works out of the box

Project Link: https://gitlab.com/Nutomic/peertube-compose/
